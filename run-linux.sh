#!/bin/bash
if [[ -z $JAVA_HOME ]]; then
  java --add-opens java.base/java.lang=ALL-UNNAMED -cp lib/corpus-services.jar:lib/corpus-service-gui-0.0.5-bundle.jar de.uni_hamburg.corpora.gui.GUIApp
else
  $JAVA_HOME/bin/java --add-opens java.base/java.lang=ALL-UNNAMED -cp lib/corpus-services.jar:lib/corpus-service-gui-0.0.5-bundle.jar de.uni_hamburg.corpora.gui.GUIApp
fi