@echo off
if defined JAVA_HOME goto :javahome
java --add-opens java.base/java.lang=ALL-UNNAMED -cp lib\corpus-services.jar;lib\corpus-service-gui-0.0.5-bundle.jar de.uni_hamburg.corpora.gui.GUIApp
goto :end
:javahome
REM remove potentially problematic quotation marks
set JAVA_HOME=%JAVA_HOME:"=%
"%JAVA_HOME%"\bin\java --add-opens java.base/java.lang=ALL-UNNAMED -cp lib\corpus-services.jar;lib\corpus-service-gui-0.0.5-bundle.jar de.uni_hamburg.corpora.gui.GUIApp
:end
pause