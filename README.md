# corpus-services-gui

A simple GUI for running the corpus services based on a YAML configuration

## Getting started

This application requires JAVA 11 to work. It can be found for download [here](https://www.oracle.com/java/technologies/downloads/).
Make sure that JAVA is set up properly using JAVA_HOME or available in the PATH. Setup instructions for Windows can be found
[here](https://explainjava.com/set-java-path-and-java-home-windows/)

## Running the GUI

Basic run scripts are included for both Windows (`run-windows.bat`) and Linux (`run-linux.sh`). The script for Mac (`run-mac.sh`) is currently the same
as the one for Linux and has not been tested.

