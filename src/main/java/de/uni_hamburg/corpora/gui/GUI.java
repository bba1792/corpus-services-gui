package de.uni_hamburg.corpora.gui;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.*;
import java.net.URL;
import java.util.function.Function;

/**
 * The definition of the GUI
 * @author bba1792, Dr. Herbert Lange
 * @version 20220802
 */
public class GUI {

    // Information on how to download the corpus-services
    private final String BRANCH = "develop-quest";
    private final String FILE_URL = "https://gitlab.rrz.uni-hamburg.de/corpus-services/corpus-services/-/jobs/artifacts/" +
            BRANCH + "/raw/target/corpus-services-1.0.jar?job=compile_withmaven";
    // The main window
    JFrame window;
    // The path to the report created by the checkers
    String reportFile;

    /**
     * Constructor setting up the GUI
     */
    public GUI() {
        window = new JFrame("Corpus-Services GUI");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel mainPanel = new JPanel(new GridLayout(5,1));
        JPanel corpusFiles = new JPanel(new GridLayout(1,3));
        mainPanel.add(new JLabel("Corpus directory"));
        final JTextField corpusPath = new JTextField();
        corpusFiles.add(corpusPath);
        final JButton corpusButton = new JButton("Select");
        corpusButton.addActionListener(actionEvent -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            // Use previous path if there is one
            if (!corpusPath.getText().isBlank()) {
                chooser.setCurrentDirectory(new File(corpusPath.getText()));
            }
            int result = chooser.showOpenDialog(corpusButton);
            if (result == JFileChooser.APPROVE_OPTION)
                corpusPath.setText(chooser.getSelectedFile().toString());
        });
        corpusFiles.add(corpusButton);
        mainPanel.add(corpusFiles);
        mainPanel.add(new JLabel("Configuration file"));
        JPanel configFile = new JPanel(new GridLayout(1,3));
        final JTextField configPath = new JTextField();
        configFile.add(configPath);
        final JButton configButton = new JButton("Select");
        configButton.addActionListener(actionEvent -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setFileFilter(new FileNameExtensionFilter(
                    "YAML configurations", "yaml", "yml"));
            // Use previous path if there is one
            if (!configPath.getText().isBlank()) {
                chooser.setCurrentDirectory(new File(configPath.getText()).getParentFile());
            }
            int result = chooser.showOpenDialog(configButton);
            if (result == JFileChooser.APPROVE_OPTION)
                configPath.setText(chooser.getSelectedFile().toString());
        });
        configFile.add(configButton);
        mainPanel.add(configFile);
        JPanel buttonPanel = new JPanel(new GridLayout(1,4));
        JButton checkButton = new JButton("Check corpus");
        buttonPanel.add(checkButton);
        JButton showReportButton = new JButton("Show report");
        showReportButton.setEnabled(false);
        buttonPanel.add(showReportButton);
        JButton saveReportButton = new JButton("Save report");
        saveReportButton.setEnabled(false);
        buttonPanel.add(saveReportButton);
        JButton updateButton = new JButton("Update corpus services");
        buttonPanel.add(updateButton);
        JButton exitButton = new JButton("Exit");
        buttonPanel.add(exitButton);
        // Handle check
        // Create callback for after the check
        Function<String, Void> callback =
                (outFile) -> {
            reportFile = outFile;
            showReportButton.setEnabled(true);
            saveReportButton.setEnabled(true);
            JOptionPane.showMessageDialog(window,
                            "Checks executed successfully. You can now check the report.",
                            "Success",
                            JOptionPane.INFORMATION_MESSAGE);
            checkButton.setEnabled(true);
            updateButton.setEnabled(true);
            return null ;
        };
        checkButton.addActionListener(actionEvent -> {
            checkButton.setEnabled(false);
            showReportButton.setEnabled(false);
            updateButton.setEnabled(false);
            // Do the checking
            try {
                Config config = Config.read(configPath.getText());
                JOptionPane.showMessageDialog(window,
                        "Starting checks now. Depending on the corpus and the checks selected this can take" +
                                " a while",
                        "Info",
                        JOptionPane.INFORMATION_MESSAGE);
                CorpusThread worker = new CorpusThread(
                        config.getName()
                        , corpusPath.getText()
                        , "tmp" // write report to tmp
                        , config.getFunctions()
                        , config.getParametersAsProperties()
                        , config.reportLimit
                        , callback);
                worker.start();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(window,
                        "Problem loading configuration file",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
                checkButton.setEnabled(true);
                updateButton.setEnabled(true);
            }
            catch (Exception | Error e) {
                JOptionPane.showMessageDialog(window,
                        "Problem " + e.getClass().toString() + " when running corpus services. " +
                                "Check the console for details",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            }
        });
        // Handle update
        updateButton.addActionListener(actionEvent -> {
            checkButton.setEnabled(false);
            updateButton.setEnabled(false);
            exitButton.setEnabled(false);
            JOptionPane.showMessageDialog(window,
                    "Download started. Depending on the internet connection this can take a few minutes",
                    "Info",
                    JOptionPane.INFORMATION_MESSAGE);
            try {
                // Download jar
                BufferedInputStream in = new BufferedInputStream(new URL(FILE_URL).openStream());
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream("lib/corpus-services.jar"));
                in.transferTo(out);
                in.close();
                out.close();
                JOptionPane.showMessageDialog(window,
                        "Download successful. You now have to restart the application.",
                        "Success",
                        JOptionPane.INFORMATION_MESSAGE);
                // End application
                window.dispose();
            } catch (IOException e) {
                //custom title, error icon
                JOptionPane.showMessageDialog(window,
                        "Error downloading file.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
            checkButton.setEnabled(true);
            updateButton.setEnabled(true);
            exitButton.setEnabled(true);
        });
        showReportButton.addActionListener(actionEvent -> {
            Desktop dt = Desktop.getDesktop();
            try {
                dt.open(new File(reportFile));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(window,
                        "Error opening report file " + reportFile + ".",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        });
        saveReportButton.addActionListener(actionEvent -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setFileFilter(new FileNameExtensionFilter(
                    "HTML file", "html", "htm"));
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.setSelectedFile(new File(reportFile));
            int result = chooser.showSaveDialog(configButton);
            if (result == JFileChooser.APPROVE_OPTION) {
                if (!(chooser.getSelectedFile().toString().toLowerCase().endsWith("htm") ||
                        chooser.getSelectedFile().toString().toLowerCase().endsWith("html")))
                    chooser.setSelectedFile(new File(chooser.getSelectedFile().toString() + ".html"));
                boolean overwrite = true;
                if (chooser.getSelectedFile().exists()) {
                    int overwriteResp =
                            JOptionPane.showConfirmDialog(window, "File " + chooser.getSelectedFile() +
                            "  already exists. Do you want to overwrite?","Overwrite",JOptionPane.YES_NO_OPTION);
                    if (overwriteResp == JOptionPane.NO_OPTION) {
                        overwrite = false;
                    }
                }
                try {
                    if (overwrite) {
                        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(reportFile));
                        BufferedOutputStream bos =
                                new BufferedOutputStream(new FileOutputStream(chooser.getSelectedFile()));
                        bis.transferTo(bos);
                        bis.close();
                        bos.close();
                        JOptionPane.showMessageDialog(window,
                                "Report saved as " + chooser.getSelectedFile().getPath() + ".",
                                "Info", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                catch (IOException e) {
                    JOptionPane.showMessageDialog(window,
                            "Error saving report to " + chooser.getSelectedFile().getPath() + ". Reason: " + e,
                            "Error", JOptionPane.ERROR_MESSAGE);
                }

            }
        });
        // Handle exit
        exitButton.addActionListener(actionEvent -> {
            // Close window
            window.dispose();
        });
        mainPanel.add(buttonPanel);
        window.getContentPane().add(mainPanel);
        window.setExtendedState(window.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        if (!new File("lib/corpus-services.jar").exists()) {
            checkButton.setEnabled(false);
            JOptionPane.showMessageDialog(window,
                    "Corpus services file corpus-services.jar is missing. Before you can use this application"
                            + " you have to update Corpus services using the button in the main window.",
                    "Warning",
                    JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * Shows the window
     */
    public void show() {
        window.setVisible(true);
    }
}
