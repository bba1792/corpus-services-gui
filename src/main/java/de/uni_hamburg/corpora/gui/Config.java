package de.uni_hamburg.corpora.gui;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class representing the YAML config created by the web interface
 */
public class Config {
    @JsonProperty("corpus")
    String name;
    @JsonProperty("checker")
    List<String> functions = new ArrayList<>();
    // Properties parameters = new Properties();
    @JsonProperty("parameters")
    Map<String,String> parameters = new HashMap<>();

    @JsonProperty("report-limit")
    int reportLimit = 0;

    public Config() {
    }

    /**
     * Reads a config file
     * @param fileName the config file
     * @return the configuration
     * @throws IOException if reading fails
     */
    public static Config read(String fileName) throws IOException {
        // Instantiating a new ObjectMapper as a YAMLFactory
        ObjectMapper om = new YAMLMapper()
                // Don't expect quotes around strings when not necessary
                .configure(YAMLGenerator.Feature.MINIMIZE_QUOTES,true)
                // Don't fail if more information than expected
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .findAndRegisterModules();
        return om.readValue(new File(fileName), Config.class);
    }

    /**
     * Writes a configuration back as a YAML string
     * @return the resulting YAML
     * @throws JsonProcessingException if conversion fails
     */
    public String write() throws JsonProcessingException {
        ObjectMapper om = new YAMLMapper()
                .configure(YAMLGenerator.Feature.MINIMIZE_QUOTES,true);
        return om.writeValueAsString(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<String> getFunctions() {
        return functions;
    }

    public void setFunctions(List<String> functions) {
        this.functions = functions;
    }

    @JsonAnyGetter
    public Map<String,String> getParameters() {
        return parameters;
    }

    @JsonAnySetter
    public void setParameters(Map<String,String> parameters) {
        this.parameters = parameters;
    }

    public Properties getParametersAsProperties() {
        Properties props = new Properties();
        props.putAll(this.parameters);
        return props;
    }

    public int getReportLimit() {
        return reportLimit;
    }

    public void setReportLimit(int reportLimit) {
        this.reportLimit = reportLimit;
    }

    @Override
    public String toString() {
        return "Config{" +
                "name='" + name + '\'' +
                ", functions=" + String.join(",", functions) +
                ", params=" + parameters +
                ", report-limit=" + reportLimit +
                '}';
    }
}
