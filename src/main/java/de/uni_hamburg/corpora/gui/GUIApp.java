package de.uni_hamburg.corpora.gui;

/**
 * Simple GUI for the corpus services
 * @author bba1792, Dr. Herbert Lange
 * @version 20220802
 */
public class GUIApp
{
    public static void main( String[] args )
    {
        // Create window
        GUI gui = new GUI();
        // Show window
        gui.show();
        // This app will continue running until the window is closed
    }
}
