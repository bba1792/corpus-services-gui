package de.uni_hamburg.corpora.gui;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.uni_hamburg.corpora.*;
import org.exmaralda.partitureditor.jexmaralda.JexmaraldaException;
import org.xml.sax.SAXException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Logger;

class CorpusThread extends Thread {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    de.uni_hamburg.corpora.Report report = new de.uni_hamburg.corpora.Report();
    String corpusName;
    String inFile ;
    List<String> functionNames;
    String outFile;
    Properties props; // The properties for the function calls
    Function<String,Void> callbackFunction;

    CorpusThread(String name, String infile, String outfile, List<String> functions, Properties properties,
                 int reportLimit, Function<String, Void> callback) {
        Report.reportLimit = reportLimit;
        this.corpusName=name;
        if (infile.equals("tmp"))
            this.inFile = System.getProperty("java.io.tmpdir") + "/corpus-files";
        else
            this.inFile = infile;
        this.functionNames = functions ;
        this.props = properties;
        if (outfile.equals("tmp")) {
            File tmpDir = new File(System.getProperty("java.io.tmpdir") + "/" + corpusName);
            // Create parent directory if it is missing
            if (!tmpDir.exists())
                tmpDir.mkdirs();
            this.outFile = tmpDir + "/checker-report.html";
        }
        else
            this.outFile = outfile;
        callbackFunction = callback;
        logger.info("Input: " + this.inFile + " output: " + this.outFile + " functions: " + functions + " params: " +
                this.props);
    }

    public void run() {
        Set<String> allFunctions = CorpusServices.getCorpusFunctions() ;
        CorpusIO cio = new CorpusIO();
        report.addNote("CorpusWebServices","Starting run at " +
                DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").format(LocalDateTime.now()));
        try {
            // Create corpus from given input file/folder
            Corpus corpus;
            if (corpusName != null && !corpusName.isEmpty()) {
                URL baseUrl = new File(inFile).toURI().toURL();
                corpus = new Corpus(corpusName,baseUrl,cio.read(baseUrl,report)) ;
            }
            else
                corpus = new Corpus(cio.read(new File(inFile).toURI().toURL(),report)) ;
            logger.info("Loaded " + corpus.getCorpusData().size() + " corpus files");
            logger.info("Got report: " + report.getFullReports());
            // For all functions to be applied, get their canonical name and create an object for them
            Set<CorpusFunction> functions = new HashSet<>() ;
            for (String function : functionNames) {
                // Indicator if we encountered the function
                boolean found = false ;
                for (String canonical : allFunctions) {
                    if (canonical.toLowerCase(Locale.ROOT).endsWith("." + function.toLowerCase(Locale.ROOT))) {
                        // Create an object from canonical name. calls the constructor with thr constructor setting hasfixingoption to false
                        try {
                            functions.add((CorpusFunction) Class.forName(canonical).getDeclaredConstructor(Properties.class).newInstance(props));
                            found = true ;
                        }
                        catch (IllegalArgumentException | NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException e) {
                            logger.severe(String.format("Error creating %s", canonical));
                            report.addWarning("CorpusWebServices", "Test " + function + " cannot be created");
                            e.printStackTrace();
                        }
                    }
                }
                if (!found) {
                    // Warn if we could not find the function
                    report.addWarning("CorpusWebServices", "Test " + function + " is not available");
                    logger.severe(String.format("Function %s is not available in corpus services", function));

                }
            }
            for (CorpusFunction f : functions) {
                logger.severe(String.format("Running function %s", f.getFunction()));
                report.addNote("CorpusWebServices", "Run test " + f.getFunction());
                de.uni_hamburg.corpora.Report result = f.execute(corpus);
                report.merge(result);
                report.addNote("CorpusWebServices", "Finish test " + f.getFunction());
                logger.severe(String.format("Done with function %s", f.getFunction()));
            }
        } catch (URISyntaxException | ClassNotFoundException | IOException | SAXException | JexmaraldaException e) {
            e.printStackTrace();
        }

        logger.info("Done with all functions");
        report.addNote("CorpusWebServices","Finished all tests at " +
                DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").format(LocalDateTime.now()));
        logger.info("Creating report");
        // Get summary
        HashMap<ReportItem.Severity,Integer> summary = CorpusServices.generateSummary(report);
        // try to convert to JSON
        String jsonSummary = "{}";
        try {
            jsonSummary = new ObjectMapper().writeValueAsString(summary);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        // Generate HTML report
        Collection<ReportItem> rawStatistics = report.getRawStatistics();
        String reportOutput = ReportItem.generateDataTableHTML(new ArrayList<>(rawStatistics),
                report.getSummaryLines());
        // Alternative: Generate XML
        //XStream xstream = new XStream();
        //String reportOutput = xstream.toXML(rawStatistics);

        logger.info("Writing report to " + outFile);
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(outFile));
            out.write(reportOutput);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("Done with report");
        callbackFunction.apply(outFile);
    }
}